Welcome to this course about nodejs using express.

his is the link of the repository for  you can been clone:

https://bitbucket.org/ing_richard/test-project-nodejs/src/develop/

step to excute this project:
1. clone the project using the branch develop
2. inside your main project directory, execute the next command 
    -> npm install
3. the last step is run the webserver with the next command
    -> npm run devstart

list of endpoints to do any actions about the API:

(GET) http://localhost:3000/api/bicicletas -> list all register
(POST) http://localhost:3000/api/bicicletas/create -> create new register 
(DELETE) http://localhost:3000/api/bicicletas/delete -> delete a register
(PUT) http://localhost:3000/api/bicicletas/update -> update a register