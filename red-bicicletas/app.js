var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasApiRouter = require('./routes/api/bicicletas');
var usuariosRouter = require('./routes/usuarios');
var usuariosApiRouter = require('./routes/api/usuarios');
var reservaApiRouter = require('./routes/api/reserva');
var tokenRouter = require('./routes/token');

var authApiRouter = require('./routes/api/auth');

var Usuario = require('./models/usuario');
var Token = require('./models/token');

const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jsonwebtoken');
const store = new session.MemoryStore;

var app = express();

// jwt secretKey
app.set('secretKey','jwt_pwd_!!aa!!a-z!!1234567890');

app.use(session({
  cookie: {maxAge: 240 * 60 * 60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicicletas!!123456*!!45252abxc'
}));

var mongoose = require('mongoose');

var mongoDB = 'mongodb://localhost/red_bicicletas';
mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB error connection: '));

//initialize passport
app.use(passport.initialize());
app.use(passport.session());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Route setup to login, logout, forgotPassword
app.get('/login', function( req, res ){
  res.render('session/login');
});

app.post('/login', function( req, res, next ){
  //passport
  passport.authenticate('local', function(err,usuario,info){
    if(err) return next(err);
    if(!usuario) return res.render('session/login',{info});
    req.logIn(usuario, function(err){
        if(err) return next(err);
        return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req,res){
  req.logout();
  res.redirect('/');
});

app.get('/forgotPassword', function(req,res){
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function(req,res,next){
	//console.log(req.body.email);
  	Usuario.findOne({email: req.body.email}, function(err, usuario){
    	console.log(usuario);
    	if(!usuario) return res.render('session/forgotPassword',{info: {message: 'No existe el email para usuario existente'}});
    
		usuario.resetPassword(function(err){
		if(err) return next(err);
		console.log('session/forgotSessionMessage');
		});
    
    	res.render('session/forgotPasswordMessage');
  
  	});
});

app.get('/resetPassword/:token', function( req, res, next ){
  	Token.findOne({token: req.params.token}, function(err,token){
		if(!token) return res.status(400).send({type:'not-verified',msg:'No existe usuario asociado al token. Verifique que su token no haya expirado'});

		Usuario.findById(token._userId, function( err, usuario ){
			if(err) return res.status(400).send({msg: 'No existe usuario asociado al token'});
			res.render('session/resetPassword', {errors:{},usuario:usuario});
		});
  	})    
});

app.post('/resetPassword', function( req,res,next ){
	if(req.body.password != req.body.confirm_password){
		res.render('session/resetPassword', {errors:{confirm_password:'No coincide con el password ingresado'}});
		return;
	}

	Usuario.findOne({email:req.body.email}, function(err,usuario){
		usuario.password = req.body.password;
		usuario.save(function(err){
			if(err){
				res.render('session/resetPassword',{errors:err.errors,usuario: new Usuario()});
			} else {
				res.redirect('/login');
			}
		});
	});
});

app.use('/', indexRouter);
app.use('/api/auth', authApiRouter);
app.use('/users', usersRouter);
app.use('/bicicletas',loggedIn,bicicletasRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasApiRouter);
app.use('/usuarios', usuariosRouter);
app.use('/api/usuarios', usuariosApiRouter);
app.use('/api/reservas', reservaApiRouter);
app.use('/token', tokenRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn( req,res,next ){
	if(req.user){
		next();
	} else {
		console.log('usuario sin logearse');
		res.redirect('/login');
	}
}

function validarUsuario(req, res, next){
	jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'),function(err, decoded){
		if(err){
			res.json({status:"error",message:err.message, data:null});
		} else {
			req.body._userId = decoded.id;

			console.log('jwt verify ' + decoded);

			next();

		}
	});
}

module.exports = app;
